import json
import requests
import singer

LOGGER = singer.get_logger()

class ZenefitsClient:
    BASE_URL = "https://api.zenefits.com"

    def __init__(self, api_key):
        self._client = requests.Session()
        self._client.headers.update({'authorization': api_key})        

    def fetch_departments(self, company_id, starting_after=None):
        url = f"{self.BASE_URL}/core/companies/{company_id}/departments"
        params = { "starting_after": starting_after } if starting_after else None        
        return self._client.get(url, params=params).json()

    def fetch_employments(self, starting_after=None):
        url = f"{self.BASE_URL}/core/employments"
        params = { "starting_after": starting_after } if starting_after else None
        return self._client.get(url, params=params).json()

    def fetch_people(self, company_id, starting_after=None):
        url = f"{self.BASE_URL}/core/companies/{company_id}/people"
        params = { "starting_after": starting_after } if starting_after else None
        return self._client.get(url, params=params).json()

    def fetch_time_durations(self, starting_after=None):
        url = f"{self.BASE_URL}/time_attendance/time_durations"
        params = { "includes" : "person person.location person.department person.company" }
        if starting_after:
            params["starting_after"] = starting_after 
        
        return self._client.get(url, params=params).json()

    def fetch_payruns(self, starting_after=None):
        url = f"{self.BASE_URL}/payroll/payruns"
        params = { "starting_after": starting_after } if starting_after else None
        return self._client.get(url, params=params).json()

    def fetch_pay_stubs(self, starting_after=None):
        url = f"{self.BASE_URL}/payroll/payrun_pay_stubs"
        params = { "starting_after": starting_after } if starting_after else None
        return self._client.get(url, params=params).json()

    def fetch_labor_groups(self, starting_after=None):
        url = f"{self.BASE_URL}/core/labor_groups"
        params = { "starting_after": starting_after } if starting_after else None
        return self._client.get(url, params=params).json()


